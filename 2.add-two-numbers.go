/*
 * @lc app=leetcode id=2 lang=golang
 *
 * [2] Add Two Numbers
 */
package main

type ListNode struct {
	Val  int
	Next *ListNode
}

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	carry := 0
	var current *ListNode
	var head *ListNode
	for l1 != nil && l2 != nil {
		val := l1.Val + l2.Val
		if carry == 1 {
			val += 1
			carry = 0
		}
		if val > 9 {
			carry = 1
			val = val - 10
		}
		node := &ListNode{Val: val, Next: nil}
		if current == nil {
			current = node
			head = node
		} else {
			current.Next = node
			current = node
		}
		l1 = l1.Next
		l2 = l2.Next

	}
	for l1 != nil {

		val := l1.Val
		if carry == 1 {
			val += 1
			carry = 0
		}
		if val > 9 {
			carry = 1
			val = val - 10
		}
		node := &ListNode{Val: val, Next: nil}
		if current == nil {
			current = node
			head = node
		} else {
			current.Next = node
			current = node
		}
		l1 = l1.Next

	}
	for l2 != nil {

		val := l2.Val
		if carry == 1 {
			val += 1
			carry = 0
		}
		if val > 9 {
			carry = 1
			val = val - 10
		}
		node := &ListNode{Val: val, Next: nil}
		if current == nil {
			current = node
			head = node
		} else {
			current.Next = node
			current = node
		}
		l2 = l2.Next
	}
	if carry == 1 {
		node := &ListNode{Val: 1, Next: nil}
		if current == nil {
			current = node
			head = node
		} else {
			current.Next = node
			current = node
		}
	}
	return head

}

// @lc code=end
