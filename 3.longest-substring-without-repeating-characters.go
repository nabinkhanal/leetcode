/*
 * @lc app=leetcode id=3 lang=golang
 *
 * [3] Longest Substring Without Repeating Characters
 */
package main

// @lc code=start
func lengthOfLongestSubstring(s string) int {
	latestOccurrence := make(map[byte]int)
	start := 0
	end := 0
	length := len(s)
	maxlen := 0
	for start < length && end < length {
		if val, ok := latestOccurrence[s[end]]; ok {
			if val >= start {
				start = val + 1
			}
		}
		latestOccurrence[s[end]] = end
		if end-start+1 > maxlen {
			maxlen = end - start + 1
		}
		end = end + 1
	}
	return maxlen
}

// @lc code=end
