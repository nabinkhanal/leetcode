/*
 * @lc app=leetcode id=1 lang=golang
 *
 * [1] Two Sum
 */
package main

// @lc code=start
func twoSum(nums []int, target int) []int {
	a := make(map[int]int)
	for i, val := range nums {
		if pos, ok := a[target-val]; ok {
			return []int{i, pos}
		} else {
			a[val] = i
		}
	}
	return []int{}
}

// @lc code=end
