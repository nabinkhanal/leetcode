/*
 * @lc app=leetcode id=4 lang=golang
 *
 * [4] Median of Two Sorted Arrays
 */
package main

import "fmt"

// @lc code=start
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	len1 := len(nums1)
	len2 := len(nums2)
	final := make([]int, len1+len2)
	i := 0
	j := 0
	for i < len1 && j < len2 {
		if nums1[i] < nums2[j] {
			final[i+j] = nums1[i]
			i += 1
		} else if nums1[i] > nums2[j] {
			final[i+j] = nums2[j]
			j += 1
		} else {
			final[i+j] = nums1[i]
			final[i+j+1] = nums2[j]
			i += 1
			j += 1
		}
	}
	for i < len1 {
		final[i+j] = nums1[i]
		i += 1
	}
	for j < len2 {
		final[i+j] = nums2[j]
		j += 1
	}
	fmt.Println("Final", final)
	totalLen := len1 + len2
	if totalLen%2 == 1 {
		return float64(final[totalLen/2])
	} else {
		return (float64(final[totalLen/2]) + float64(final[totalLen/2-1]))/2
	}
}

// @lc code=end
